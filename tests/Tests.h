#include <QObject>

class Tests : public QObject
{
  Q_OBJECT
private slots:
  void testQtCpp();
  void testStdCpp();
};
