#include "Tests.h"

#include <sstream>

#include <QtTest>

void Tests::testQtCpp()
{
  int max = 5;

  QString output;
  QTextStream stream(&output, QIODevice::WriteOnly);
  #include "string_cppstream.h"

  QCOMPARE(output, "1, 2, 3, 4, 5");
}

void Tests::testStdCpp()
{
  int max = 7;

  std::stringstream stream;  
  #include "string_cppstream.h"

  QCOMPARE(stream.str(), "1, 2, 3, 4, 5, 6, 7");
}

QTEST_MAIN(Tests)
