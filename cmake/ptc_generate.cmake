# Generate a header with ptc based on SOURCE
# MODE is qtcpp or stdcpp
# SOURCE is the source (relative to CMAKE_CURRENT_SOURCE_DIR)
# DESTINATION is the name of the destination (will be created in the CMAKE_CURRENT_BINARY_DIR)
# DEPENDENCY is the name of a file that includes the generated header

function(PTC_GENERATE MODE SOURCE DESTINATION ) # DEPENDENCY
  if(NOT MODE STREQUAL "qtcpp" AND NOT MODE STREQUAL "cppstream")
    set(DEPENDENCY ${DESTINATION})
    set(DESTINATION ${SOURCE})
    set(SOURCE ${MODE})
    set(MODE cppstream)
    message(WARNING "Calling ptc_generate without MODE is deprecated and will be removed in a future version.")
  else()
    set(DEPENDENCY ${ARGV3})
  endif()
  get_filename_component(DESTINATION_DIR "${CMAKE_CURRENT_BINARY_DIR}/${DESTINATION}" PATH)  
  file(MAKE_DIRECTORY ${DESTINATION_DIR})

  add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${DESTINATION}
                     DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${SOURCE} ${PTC_EXECUTABLE}
                     COMMAND ${PTC_EXECUTABLE} ${MODE} ${CMAKE_CURRENT_SOURCE_DIR}/${SOURCE} ${CMAKE_CURRENT_BINARY_DIR}/${DESTINATION}
  )

set_property(SOURCE ${DEPENDENCY} APPEND PROPERTY OBJECT_DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${DESTINATION} )

endfunction()
