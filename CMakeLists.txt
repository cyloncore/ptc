project(ptc)

cmake_minimum_required(VERSION 3.10)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules)

set(CMAKE_CXX_STANDARD 17)

include(cmake/ptc_generate.cmake)

########################
## Define version

set(PTC_MAJOR_VERSION 2)
set(PTC_MINOR_VERSION 0)
set(PTC_PATCH_VERSION 0)
set(PTC_VERSION ${PTC_MAJOR_VERSION}.${PTC_MINOR_VERSION}.${PTC_PATCH_VERSION})

########################
## Define install dirs

set(INSTALL_LIB_DIR     lib${LIB_SUFFIX}/ CACHE PATH "Installation directory for libraries")
set(INSTALL_INCLUDE_DIR include/          CACHE PATH "Installation directory for headers")
set(INSTALL_BIN_DIR     bin/              CACHE PATH "Installation directory for executables")
set(INSTALL_SHARE_DIR   share/ptc/         CACHE PATH "Installation directory for data")
set(INSTALL_DOC_DIR     share/doc/ptc/     CACHE PATH "Installation directory for documentation")

if(WIN32 AND NOT CYGWIN)
  set(DEF_INSTALL_CMAKE_DIR cmake)
else()
  set(DEF_INSTALL_CMAKE_DIR lib/cmake/ptc)
endif()
set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR} CACHE PATH "Installation directory for CMake files")

# Make relative paths absolute (needed later on)
foreach(p LIB BIN INCLUDE SHARE DOC CMAKE)
  set(var INSTALL_${p}_DIR)
  if(NOT IS_ABSOLUTE "${${var}}")
    set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
  endif()
endforeach()

#
# Define INSTALL_TARGETS_DEFAULT_ARGS to be used as install target for program and library.
# It will do the right thing on all platform
#
set(INSTALL_TARGETS_DEFAULT_ARGS  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin
                                  LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT shlib
                                  ARCHIVE DESTINATION "${INSTALL_LIB_DIR}" COMPONENT Devel )

########################
## Enable all Warnings
if(MSVC)
  add_compile_options(/W4 /WX)
else()
  add_compile_options(-Wall -Wextra -pedantic -Werror)
endif()

########################
## Enable testing

enable_testing()

########################
## Find Qt6::Core

find_package(Qt6 REQUIRED COMPONENTS Core Test)
set(CMAKE_AUTOMOC ON)

########################
## Subdirectories

add_subdirectory(src)
add_subdirectory(examples)
add_subdirectory(tests)

########################
## Config files

export(TARGETS ${PROJECT_EXPORTED_TARGETS}
  FILE "${PROJECT_BINARY_DIR}/ptcTargets.cmake")

include(CMakePackageConfigHelpers)
configure_package_config_file(ptcConfig.cmake.in "${PROJECT_BINARY_DIR}/ptcConfig.cmake"
    INSTALL_DESTINATION ${INSTALL_CMAKE_DIR}
    PATH_VARS INSTALL_BIN_DIR
    )
write_basic_package_version_file("${PROJECT_BINARY_DIR}/ptcConfigVersion.cmake" VERSION ${PTC_VERSION} COMPATIBILITY SameMajorVersion)

# Install the ptcConfig.cmake and ptcConfigVersion.cmake
install(FILES
  cmake/ptc_generate.cmake
  "${PROJECT_BINARY_DIR}/ptcConfig.cmake"
  "${PROJECT_BINARY_DIR}/ptcConfigVersion.cmake"
  DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)

# Install the export set for use with the install-tree
install(EXPORT ptcTargets DESTINATION
  "${INSTALL_CMAKE_DIR}" COMPONENT dev)
