#ifndef ABSTRACTGENERATOR_H
#define ABSTRACTGENERATOR_H

#include <QByteArray>

namespace ptc
{
  class AbstractGenerator
  {
  public:
    struct MetaInformation
    {
      QByteArray filename;
      int line;
    };
  public:
    AbstractGenerator();
    virtual void processTemplate(const MetaInformation& _info, const QByteArray& _template) = 0;
    virtual void processCode(const MetaInformation& _info, const QByteArray& _code) = 0;
    virtual void processCodeInTemplate(const MetaInformation& _info, const QByteArray& _code) = 0;
    virtual void processCodeInTemplate(const MetaInformation& _info, const QByteArray& _capture, const QByteArray& _code) = 0;
  };
}

#endif // ABSTRACTGENERATOR_H
