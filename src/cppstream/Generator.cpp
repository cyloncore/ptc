#include "Generator.h"
#include <QIODevice>

using namespace ptc::cppstream;

Generator::Generator(QIODevice* _output) : m_device(_output)
{
}

void Generator::processCode(const MetaInformation& _info, const QByteArray& _code)
{
  writeMeta(_info);
  m_device->write(_code);
  m_device->write("\n");
}

void Generator::processCodeInTemplate(const MetaInformation& _info, const QByteArray& _code)
{
  writeMeta(_info);
  m_device->write("stream << (");
  m_device->write(_code);
  m_device->write(");\n");
}

void Generator::processCodeInTemplate(const MetaInformation& _info, const QByteArray& _capture, const QByteArray& _code)
{
  writeMeta(_info);
  m_device->write("stream << ([");
  m_device->write(_capture);
  m_device->write("]() {");
  m_device->write(_code);
  m_device->write("})();\n");
}

void Generator::processTemplate(const MetaInformation& _info, const QByteArray& _template)
{
  writeMeta(_info);
  QByteArray temp = _template;
  temp.replace('\\', "\\\\");
  temp.replace('"', "\\\"");
  temp.replace("\\\\\"", "\"");
  temp.replace('\n', "\\n\"\n\"");
  m_device->write("stream << \"");
  m_device->write(temp);
  m_device->write("\";\n");
}

void Generator::writeMeta ( const ptc::AbstractGenerator::MetaInformation& _info )
{
  m_device->write("#line ");
  m_device->write(QByteArray::number(_info.line));
  m_device->write(" \"");
  m_device->write(_info.filename);
  m_device->write("\"\n");
}
