#ifndef GENERATOR_H
#define GENERATOR_H

#include "AbstractGenerator.h"

class QIODevice;

namespace ptc::cppstream
{
  class Generator : public AbstractGenerator
  {
  public:
    Generator(QIODevice* _output);
    void processCode(const MetaInformation& _info, const QByteArray& _code) override;
    void processCodeInTemplate(const MetaInformation& _info, const QByteArray& _code) override;
    void processCodeInTemplate(const MetaInformation& _info, const QByteArray& _capture, const QByteArray& _code) override;
    void processTemplate(const MetaInformation& _info, const QByteArray& _template) override;
  private:
    void writeMeta(const MetaInformation& _info);
  private:
    QIODevice* m_device;
  };
}

#endif // GENERATOR_H
