#include <iostream>

#include <QDebug>
#include <QFile>
#include <QFileInfo>

#include "AbstractGenerator.h"
#include "TemplateDefinitionProcessor.h"
#include "cppstream/Generator.h"

int main(int _argc, char** _argv)
{
  if(_argc != 4)
  {
    qWarning() << "ptc [codebackend] [input] [output]";
    return -1;
  }
  
  QString codebackend_str = _argv[1];
  QString input_filename  = _argv[2];
  QString output_filename = _argv[3];
  
  QFile input_file(input_filename);
  if(not input_file.open(QIODevice::ReadOnly))
  {
    qWarning() << "Cannot open file " << input_filename << " for writting.";
    return -1;
  }
  
  QFile output_file(output_filename);
  if(not output_file.open(QIODevice::WriteOnly))
  {
    qWarning() << "Cannot open file " << output_filename << " for writting.";
    return -1;
  }
  
  ptc::AbstractGenerator* ag = 0;
  if(codebackend_str == "qtcpp" or codebackend_str == "cppstream")
  {
    ag = new ptc::cppstream::Generator(&output_file);
  }
  if(not ag)
  {
    qWarning() << "Unknown  code generator " << codebackend_str;
    return -1;
  }
  
  ptc::TemplateDefinitionProcessor tdp(QFileInfo(input_filename).absoluteFilePath().toUtf8(), &input_file, ag);
  if(tdp.process())
  {
    return 0;
  } else {
    qWarning() << "Processing failed";
  }
}
