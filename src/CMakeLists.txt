include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(PTC_SRCS 
  main.cpp
  TemplateDefinitionProcessor.cpp
  AbstractGenerator.cpp
  cppstream/Generator.cpp )

add_executable(ptc ${PTC_SRCS})
target_link_libraries(ptc Qt6::Core)

if(APPLE)
  set_target_properties(ptc
    PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE
  )
endif()

install(TARGETS ptc EXPORT ptcTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )

set(PTC_EXECUTABLE ${CMAKE_CURRENT_BINARY_DIR}/ptc PARENT_SCOPE)