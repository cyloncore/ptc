#ifndef TEMPLATEDEFINITIONPROCESSOR_H
#define TEMPLATEDEFINITIONPROCESSOR_H

class QByteArray;
class QChar;
class QIODevice;

namespace ptc
{
  class AbstractGenerator;
  class TemplateDefinitionProcessor
  {
  public:
    TemplateDefinitionProcessor(const QByteArray& _filename, QIODevice* _device, AbstractGenerator* _generator);
    bool process();
  private:
    char getNextChar();
    bool eof() const;
    QByteArray readUntilNextEnd();
    QByteArray readCapture();
  private:
    struct Private;
    Private* const d;
  };
}

#endif // TEMPLATEDEFINITIONPROCESSOR_H
