#include "TemplateDefinitionProcessor.h"
#include "AbstractGenerator.h"
#include <QIODevice>

using namespace ptc;

struct TemplateDefinitionProcessor::Private
{
  QByteArray filename;
  QIODevice* device;
  AbstractGenerator* generator;
  int line, col;
};

TemplateDefinitionProcessor::TemplateDefinitionProcessor(const QByteArray& _filename, QIODevice* _device, AbstractGenerator* _generator) : d(new Private)
{
  d->filename   = _filename;
  d->device     = _device;
  d->generator  = _generator;
  d->line       = 1;
  d->col        = 1;
}

bool TemplateDefinitionProcessor::process()
{
  QByteArray current;
  int current_line = d->line;
  while(not eof())
  {
    char c = getNextChar();
    while(c == '/')
    {
      char c2 = getNextChar();
      if(c2 == '%')
      {
        if(current.size() > 0)
        {
          d->generator->processTemplate({d->filename, current_line}, current);
          current.clear();
        }
        char c3 = getNextChar();
        switch(c3)
        {
          case '=':
          {
            int l = d->line;
            d->generator->processCodeInTemplate({d->filename, l}, readUntilNextEnd());
            break;
          }
          case '[':
          {
            QByteArray capture = readCapture();
            getNextChar(); // should be an equal
            int l = d->line;
            d->generator->processCodeInTemplate({d->filename, l}, capture, readUntilNextEnd());
            break;
          }
          default:
          {
            int l = d->line;
            if(c3 == '\n') --l;
            d->generator->processCode({d->filename, l}, c3 + readUntilNextEnd());
            break;
          }
        }
        c = getNextChar();
      } else {
        if(c != 0)
        {
          current += c;
        }
        c = c2;
      }
    }
    if(current.isEmpty())
    {
      current_line = d->line;
    }
    if(c != 0)
    {
      current += c;
    }
  }
  if(current.size() > 0)
  {
    d->generator->processTemplate({d->filename, d->line}, current);
    current.clear();
  }
  return true;
}

char TemplateDefinitionProcessor::getNextChar()
{
  char nc;
  if(d->device->getChar(&nc))
  {
    if( nc == '\n' )
    {
      ++d->line;
      d->col = 1;
    } else {
      ++d->col;
    }
  } else {
    return 0;
  }
  return nc;
}

bool TemplateDefinitionProcessor::eof() const
{
  return d->device->atEnd();
}

QByteArray TemplateDefinitionProcessor::readCapture()
{
  QByteArray capture;
  while(not eof())
  {
    char c = getNextChar();
    if(c == ']') break;
    capture += c;
  }
  return capture;
}

QByteArray TemplateDefinitionProcessor::readUntilNextEnd()
{
  QByteArray capture;
  while(not eof())
  {
    char c = getNextChar();
    if(c == '%')
    {
      char c2 = getNextChar();
      if(c2 == '/')
      {
        break;
      } else {
        capture += c;
        capture += c2;
      }
    } else {
      capture += c;
    }
  }
  return capture;
}
